CREATE TRIGGER init_person_uuid
BEFORE INSERT
    ON person FOR EACH ROW
    
BEGIN
  IF new.id IS NULL THEN
    SET new.id = uuid();
  END IF;
END;