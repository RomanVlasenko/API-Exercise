package com.romanvlasenko.peopleapi.api

import com.romanvlasenko.peopleapi.model.CreateOrUpdatePersonDTO
import com.romanvlasenko.peopleapi.model.PersonDTO
import com.romanvlasenko.peopleapi.service.PersonService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping("/people")
class PeopleResource(@Autowired private val personService: PersonService) {

    @GetMapping(consumes = [MediaType.ALL_VALUE])
    fun getPeople(): List<PersonDTO> = personService.getPeople()

    @PostMapping(consumes = [MediaType.APPLICATION_JSON_VALUE])
    @ResponseStatus(HttpStatus.CREATED)
    fun createPerson(@RequestBody person: CreateOrUpdatePersonDTO): PersonDTO = personService.createPerson(person)

    @GetMapping(path = ["/{uuid}"], consumes = [MediaType.APPLICATION_JSON_VALUE])
    fun getPerson(@PathVariable uuid: UUID): PersonDTO = personService.getPersonById(uuid)

    @PutMapping(path = ["/{uuid}"], consumes = [MediaType.APPLICATION_JSON_VALUE])
    fun updatePerson(@PathVariable uuid: UUID, @RequestBody person: CreateOrUpdatePersonDTO): PersonDTO =
            personService.update(uuid, person)

    @DeleteMapping(path = ["/{uuid}"], consumes = [MediaType.APPLICATION_JSON_VALUE])
    @ResponseStatus(HttpStatus.OK)
    fun deletePerson(@PathVariable uuid: UUID) = personService.deletePerson(uuid)

}
