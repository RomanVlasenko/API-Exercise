package com.romanvlasenko.peopleapi

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication


@SpringBootApplication
class PeopleApiApplication

fun main(args: Array<String>) {
	runApplication<PeopleApiApplication>(*args)
}
