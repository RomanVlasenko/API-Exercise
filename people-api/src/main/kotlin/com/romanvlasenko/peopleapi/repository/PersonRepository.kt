package com.romanvlasenko.peopleapi.repository

import com.romanvlasenko.peopleapi.model.Person
import org.springframework.data.repository.Repository
import java.util.*

interface PersonRepository : Repository<Person, UUID> {

    fun save(person: Person): Person

    fun findAll(): List<Person>

    fun findById(id: UUID): Person?

    fun existsById(id: UUID): Boolean

    fun deleteById(id: UUID)

}
