package com.romanvlasenko.peopleapi.model

import javax.validation.constraints.Size

data class CreateOrUpdatePersonDTO(var survived: Boolean?,
                                   var passengerClass: Int?,
                                   @Size(max = 100) var name: String?,
                                   var sex: SexEnum?,
                                   var age: Int?,
                                   var siblingsOrSpousesAboard: Int?,
                                   var parentsOrChildrenAboard: Int?,
                                   var fare: Double?)