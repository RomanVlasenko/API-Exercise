package com.romanvlasenko.peopleapi.model

import com.fasterxml.jackson.annotation.JsonProperty

enum class SexEnum {
    @JsonProperty("male")
    MALE,

    @JsonProperty("female")
    FEMALE,

    @JsonProperty("other")
    OTHER;

}