package com.romanvlasenko.peopleapi.model

import org.hibernate.annotations.Type
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "person")
data class Person(
        @Column(name = "id") @Type(type = "uuid-char") @Id val id: UUID? = null,
        @Column(name = "survived") val survived: Boolean?,
        @Column(name = "passenger_class") val passengerClass: Int?,
        @Column(name = "name") val name: String?,
        @Enumerated(EnumType.STRING) @Column(name = "sex") val sex: SexEnum?,
        @Column(name = "age") val age: Int?,
        @Column(name = "siblings_or_spouses_aboard") val siblingsOrSpousesAboard: Int?,
        @Column(name = "parents_or_children_aboard") val parentsOrChildrenAboard: Int?,
        @Column(name = "fare") val fare: Double?
) {
    fun toDTO(): PersonDTO = PersonDTO(
            uuid = this.id,
            survived = this.survived,
            passengerClass = this.passengerClass,
            name = this.name,
            sex = this.sex,
            age = this.age,
            siblingsOrSpousesAboard = this.siblingsOrSpousesAboard,
            parentsOrChildrenAboard = this.parentsOrChildrenAboard,
            fare = this.fare)

    companion object {

        fun fromDTO(uuid: UUID, person: CreateOrUpdatePersonDTO) = Person(
                id = uuid,
                survived = person.survived,
                passengerClass = person.passengerClass,
                name = person.name,
                sex = person.sex,
                age = person.age,
                siblingsOrSpousesAboard = person.siblingsOrSpousesAboard,
                parentsOrChildrenAboard = person.parentsOrChildrenAboard,
                fare = person.fare)

        fun fromDTO(person: CreateOrUpdatePersonDTO) = Person(
                id = UUID.randomUUID(),
                survived = person.survived,
                passengerClass = person.passengerClass,
                name = person.name,
                sex = person.sex,
                age = person.age,
                siblingsOrSpousesAboard = person.siblingsOrSpousesAboard,
                parentsOrChildrenAboard = person.parentsOrChildrenAboard,
                fare = person.fare)

    }

}
