package com.romanvlasenko.peopleapi.model

import java.util.*

data class PersonDTO(var uuid: UUID? = null,
                     var survived: Boolean?,
                     var passengerClass: Int?,
                     var name: String?,
                     var sex: SexEnum?,
                     var age: Int?,
                     var siblingsOrSpousesAboard: Int?,
                     var parentsOrChildrenAboard: Int?,
                     var fare: Double?)