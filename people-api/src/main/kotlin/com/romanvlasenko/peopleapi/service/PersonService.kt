package com.romanvlasenko.peopleapi.service

import com.romanvlasenko.peopleapi.model.CreateOrUpdatePersonDTO
import com.romanvlasenko.peopleapi.model.Person
import com.romanvlasenko.peopleapi.model.PersonDTO
import com.romanvlasenko.peopleapi.repository.PersonRepository
import com.romanvlasenko.peopleapi.service.exception.PersonNotFoundException
import org.springframework.stereotype.Service
import java.util.*

@Service
class PersonService(private val personRepository: PersonRepository) {
    fun getPeople(): List<PersonDTO> {
        return personRepository.findAll().map { person -> person.toDTO() }
    }

    fun createPerson(person: CreateOrUpdatePersonDTO): PersonDTO {
        return personRepository.save(Person.fromDTO(person)).toDTO()
    }

    fun getPersonById(uuid: UUID): PersonDTO {
        val person = personRepository.findById(uuid) ?: throw PersonNotFoundException(uuid)
        return person.toDTO()
    }

    fun update(uuid: UUID, updatedPerson: CreateOrUpdatePersonDTO): PersonDTO {
        if (!personRepository.existsById(uuid)) {
            throw PersonNotFoundException(uuid)
        }

        return personRepository.save(Person.fromDTO(uuid, updatedPerson)).toDTO()
    }

    fun deletePerson(uuid: UUID) {
        if (!personRepository.existsById(uuid)) {
            throw PersonNotFoundException(uuid)
        }

        personRepository.deleteById(uuid)
    }

}