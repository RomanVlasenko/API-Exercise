package com.romanvlasenko.peopleapi.service.exception

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus
import java.util.*

@ResponseStatus(HttpStatus.NOT_FOUND)
class PersonNotFoundException(uuid: UUID) : RuntimeException("Person not found: uuid=$uuid")